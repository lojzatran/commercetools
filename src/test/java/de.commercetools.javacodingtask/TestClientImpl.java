package de.commercetools.javacodingtask;

import de.commercetools.javacodingtask.client.Client;
import de.commercetools.javacodingtask.client.ImportResults;
import de.commercetools.javacodingtask.models.Customer;
import de.commercetools.javacodingtask.models.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Testing client which saves the entities
 * in its fields for later reading
 *
 * @author lojzatran
 */
public class TestClientImpl implements Client {
    private static final Logger LOG = LoggerFactory.getLogger(TestClientImpl.class);

    private List<Customer> customers;

    private List<Order> orders;

    @Override
    public ImportResults importCustomer(List<Customer> customers) {
        this.customers = new ArrayList<Customer>(customers);
        return new ImportResults();
    }

    @Override
    public ImportResults importOrders(List<Order> orders) {
        this.orders = new ArrayList<Order>(orders);
        return new ImportResults();
    }

    @Override
    public void close() throws IOException {
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public List<Order> getOrders() {
        return orders;
    }
}