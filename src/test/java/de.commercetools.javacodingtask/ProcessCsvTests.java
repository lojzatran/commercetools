package de.commercetools.javacodingtask;

import de.commercetools.javacodingtask.csvprocessor.impl.CommercetoolsCsvProcessor;
import de.commercetools.javacodingtask.errors.ServiceUnavailableException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Tests for {@link de.commercetools.javacodingtask.csvprocessor.impl.CommercetoolsCsvProcessor}
 *
 * @author lojzatran
 */
public class ProcessCsvTests {
    private static final Logger LOG = LoggerFactory.getLogger(ProcessCsvTests.class);


    private static final String CSV = "seq,first,last,age,street,city,state,zip,dollar,pick\n" +
            "1,Emma,Silva,6,Nuteb Extension,Irupota,GA,04563,$5855.06,WHITE\n" +
            "2,Angela,Johnston,93,Moac Key,Eggutaf,VT,15513,$5853.29,GREEN\n" +
            "3,Travis,Morris,25,Zane Extension,Ganwobzel,FM,79522,$1142.69,RED\n" +
            "4,Cora,George,8,Zukuru Pass,Guwsugsa,ME,51723,$4467.79,GREEN\n" +
            "5,Aaron,Williamson,79,Vonzo Drive,Fegortow,ND,99984,$223.38,GREEN\n" +
            "6,Scarlett,Wilkins,110,Azdi Turnpike,Parunup,IN,01486,$852.18,RED\n" +
            "7,Bennett,Haynes,90,Jaeho Trail,Obeonewi,NV,45558,$3914.81,YELLOW";

    private static final int NUMBER_OF_CSV_LINES = 8;
    private static final int NUMBER_OF_CSV_HEADER_LINES = 1;

    private ByteArrayInputStream is;
    private BufferedReader br;

    @Before
    public void setUp() {
        // convert String into InputStream
        is = new ByteArrayInputStream(CSV.getBytes());
        // read it with BufferedReader
        br = new BufferedReader(new InputStreamReader(is));
    }

    @After
    public void cleanUp() throws IOException {
        is.close();
        br.close();
    }

    @Test
    public void whenWeInsertCsvIntoProcessor_itShouldSendEntitiesAfterThresholdIsReached() throws Exception {
        TestClientImpl testClient = new TestClientImpl();

        LOG.debug("Set up the csv processor for testing");
        int importThreshold = NUMBER_OF_CSV_LINES - NUMBER_OF_CSV_HEADER_LINES;

        CommercetoolsCsvProcessor csvProcessor = new CommercetoolsCsvProcessor();
        csvProcessor.setClient(testClient);
        csvProcessor.setImportThreshold(importThreshold);

        LOG.debug("Process CSV file");
        csvProcessor.processCsv(br);

        LOG.debug("The client flushes to server when the threshold is reached");
        Assert.assertEquals(importThreshold, testClient.getCustomers().size());
        Assert.assertEquals(importThreshold, testClient.getOrders().size());
    }

    @Test
    public void whenWeHaveShorterCsvThanThreshold_itShouldSendEntitiesAfterCsvEnds() throws Exception {
        TestClientImpl testClient = new TestClientImpl();

        LOG.debug("Set up the threshold to a larger number than csv lines");
        int importThreshold = NUMBER_OF_CSV_LINES + 3;

        CommercetoolsCsvProcessor csvProcessor = new CommercetoolsCsvProcessor();
        csvProcessor.setClient(testClient);
        csvProcessor.setImportThreshold(importThreshold);

        LOG.debug("Process CSV file");
        csvProcessor.processCsv(br);

        LOG.debug("The client flushes to server when EOF is reached");
        int numberOfEntityLines = NUMBER_OF_CSV_LINES - NUMBER_OF_CSV_HEADER_LINES;
        Assert.assertEquals(numberOfEntityLines, testClient.getCustomers().size());
        Assert.assertEquals(numberOfEntityLines, testClient.getOrders().size());
    }

    @Test
    public void whenServiceIsUnavailableErrorIsThrown_itShouldRepeatProcessing() throws Exception {
        int numberOfErrors = 1;
        ExceptionThrowTestClientImpl testClient = new ExceptionThrowTestClientImpl();
        testClient.setException(new ServiceUnavailableException());
        testClient.setMaxErrorThreshold(1);

        LOG.debug("Set up the threshold");
        int importThreshold = NUMBER_OF_CSV_LINES - NUMBER_OF_CSV_HEADER_LINES;

        CommercetoolsCsvProcessor csvProcessor = new CommercetoolsCsvProcessor();
        csvProcessor.setClient(testClient);
        csvProcessor.setImportThreshold(importThreshold);
        csvProcessor.setMaxErrorRetries(-1);

        LOG.debug("Process CSV file");
        csvProcessor.processCsv(br);

        LOG.debug("The client flushes to server when EOF is reached");
        Assert.assertEquals(importThreshold, testClient.getCustomers().size());
        Assert.assertEquals(importThreshold, testClient.getOrders().size());
        Assert.assertEquals(numberOfErrors, testClient.getErrorCounter());
    }

    @Test(expected = RuntimeException.class)
    public void whenOtherExceptionIsThrown_itShouldEndProcessing() throws Exception {
        ExceptionThrowTestClientImpl testClient = new ExceptionThrowTestClientImpl();
        testClient.setException(new RuntimeException());

        LOG.debug("Set up the threshold");
        int importThreshold = NUMBER_OF_CSV_LINES - NUMBER_OF_CSV_HEADER_LINES;

        CommercetoolsCsvProcessor csvProcessor = new CommercetoolsCsvProcessor();
        csvProcessor.setClient(testClient);
        csvProcessor.setImportThreshold(importThreshold);
        csvProcessor.setMaxErrorRetries(-1);

        LOG.debug("Process CSV file");
        csvProcessor.processCsv(br);
    }
}