package de.commercetools.javacodingtask;

import de.commercetools.javacodingtask.client.ImportResults;
import de.commercetools.javacodingtask.models.Customer;
import de.commercetools.javacodingtask.models.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * This client throws {@link RuntimeException} when calling {@link #importCustomer(List)}
 * until the counter reached threshold (default to 1). After that, it stops throwing exceptions
 * and it saves customer list into its field.
 *
 * @author lojzatran
 */
public class ExceptionThrowTestClientImpl extends TestClientImpl {
    private static final Logger LOG = LoggerFactory.getLogger(ExceptionThrowTestClientImpl.class);

    private RuntimeException exception;

    private int errorCounter;

    private int maxErrorThreshold = 1;

    @Override
    public ImportResults importCustomer(List<Customer> customers) {
        if (errorCounter < maxErrorThreshold) {
            errorCounter += 1;
            throw exception;
        } else {
            return super.importCustomer(customers);
        }
    }

    @Override
    public ImportResults importOrders(List<Order> orders) {
        return super.importOrders(orders);
    }

    @Override
    public void close() throws IOException {
    }

    public void setException(RuntimeException e) {
        this.exception = e;
    }

    public void setMaxErrorThreshold(int maxErrorThreshold) {
        this.maxErrorThreshold = maxErrorThreshold;
    }

    public int getErrorCounter() {
        return errorCounter;
    }
}