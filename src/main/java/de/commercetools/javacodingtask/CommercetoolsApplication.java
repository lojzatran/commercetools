package de.commercetools.javacodingtask;

import de.commercetools.javacodingtask.client.Client;
import de.commercetools.javacodingtask.client.ClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CommercetoolsApplication {

    private static final Logger LOG = LoggerFactory.getLogger(CommercetoolsApplication.class);

    @Value("${commercetools.csv.applicantEmail:lojzatran@gmail.com}")
    private String applicantEmail;

    public static void main(String[] args) {
        SpringApplication.run(CommercetoolsApplication.class, args);
    }

    @Bean
    public Client getClient() {
        return ClientFactory.create(applicantEmail);
    }
}
