package de.commercetools.javacodingtask.home;

import de.commercetools.javacodingtask.csvprocessor.CsvProcessor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Home page controller
 *
 * @author lojzatran
 */
@RestController
public class HomeController {
    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    @Value("${commercetools.csv.location:http://s3-eu-west-1.amazonaws.com/commercetools-datastore/codingtasks/random-customer-order-testdata-1.csv}")
    public String csvLocation;

    @Autowired
    private CsvProcessor csvProcessor;

    /**
     * Fetch the CSV file and send it to the CSV processor
     *
     * @return ResponseEntity {@link HttpStatus#OK} if everything goes without error,
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} if there are errors during processing
     * @throws IOException
     * @throws InterruptedException
     */
    @RequestMapping("/")
    public ResponseEntity index() throws IOException, InterruptedException {
        HttpEntity entity = getHttpEntity();
        if (entity != null) {
            InputStream inputStream = null;
            InputStreamReader is = null;
            BufferedReader br = null;
            try {
                inputStream = entity.getContent();
                is = new InputStreamReader(inputStream);
                br = new BufferedReader(is);

                csvProcessor.processCsv(br);
            } catch (Exception e) {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            } finally {
                if (br != null) {
                    br.close();
                }
                if (is != null) {
                    is.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            }
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    private HttpEntity getHttpEntity() throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(csvLocation);
        HttpResponse response = httpClient.execute(httpGet);
        return response.getEntity();
    }
}