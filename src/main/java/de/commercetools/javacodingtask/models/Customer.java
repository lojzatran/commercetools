package de.commercetools.javacodingtask.models;

import io.sphere.sdk.models.Base;

/**
 * A customer of an online shop.
 */
public final class Customer extends Base {
    private String id;
    private String firstName;
    private String lastName;
    private int age;
    private String street;
    private String city;
    private String state;
    private String zip;

    public Customer() {
    }

    /**
     * Create a {@link Customer} object from CSV line
     *
     * @param csvLine CSV line in following sequence: seq,first,last,age,street,city,state,zip
     * @return parsed Customer object
     */
    public static Customer parseCustomerFromCsv(String csvLine) {
        String[] customerCsvSplit = csvLine.split(",");
        Customer customer = new Customer();
        customer.setId(customerCsvSplit[0]);
        customer.setFirstName(customerCsvSplit[1]);
        customer.setLastName(customerCsvSplit[2]);
        customer.setAge(Integer.parseInt(customerCsvSplit[3]));
        customer.setStreet(customerCsvSplit[4]);
        customer.setCity(customerCsvSplit[5]);
        customer.setState(customerCsvSplit[6]);
        customer.setZip(customerCsvSplit[7]);

        return customer;
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id='" + id + '\'' +
                '}';
    }
}
