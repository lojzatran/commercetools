package de.commercetools.javacodingtask.models;

import com.google.common.math.DoubleMath;
import io.sphere.sdk.models.Base;

import java.math.RoundingMode;
import java.util.Currency;
import java.util.Locale;
import java.util.UUID;

/**
 * The order of a customer in an online shop.
 */
public class Order extends Base {
    private String customerId;
    private String id;
    private String pick;
    private Currency currency;
    private long centAmount;

    public Order() {
    }

    /**
     * Create a {@link Order} object from CSV line
     *
     * @param csvLine CSV line in following sequence: seq,first,last,age,street,city,state,zip,dollar,pick
     * @return parsed Order object
     */
    public static Order parseOrderFromCsv(String csvLine, String customerId) {
        String[] orderCsvSplit = csvLine.split(",");
        Order order = new Order();
        order.setCustomerId(customerId);
        order.setId(UUID.randomUUID().toString());
        order.setCurrency(Currency.getInstance(Locale.US));
        String dollarWithSign = orderCsvSplit[8];
        Double dollar = Double.parseDouble(dollarWithSign.substring(1));
        order.setCentAmount(DoubleMath.roundToLong(dollar * 100, RoundingMode.HALF_UP));
        order.setPick(orderCsvSplit[9]);

        return order;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(final String personId) {
        this.customerId = personId;
    }

    public long getCentAmount() {
        return centAmount;
    }

    public void setCentAmount(final long centAmount) {
        this.centAmount = centAmount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    public String getPick() {
        return pick;
    }

    public void setPick(final String pick) {
        this.pick = pick;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", customerId='" + customerId + '\'' +
                '}';
    }
}
