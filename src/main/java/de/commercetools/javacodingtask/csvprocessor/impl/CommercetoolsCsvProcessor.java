package de.commercetools.javacodingtask.csvprocessor.impl;

import com.google.common.annotations.VisibleForTesting;
import de.commercetools.javacodingtask.client.Client;
import de.commercetools.javacodingtask.csvprocessor.CsvProcessor;
import de.commercetools.javacodingtask.errors.ServiceUnavailableException;
import de.commercetools.javacodingtask.models.Customer;
import de.commercetools.javacodingtask.models.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.util.LinkedList;
import java.util.List;

/**
 * CSV processor for Commercetools
 *
 * @author lojzatran
 */
@Service
public class CommercetoolsCsvProcessor implements CsvProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(CommercetoolsCsvProcessor.class);

    @Value("${commercetools.csv.importThreshold:500}")
    private int importThreshold;

    @Value("${commercetools.csv.maxErrorRetries:-1}")
    private int maxErrorRetries;

    @Autowired
    private Client client;

    @Override
    public void processCsv(BufferedReader br) throws Exception {
        List<Customer> customerList = new LinkedList<Customer>();
        List<Order> orderList = new LinkedList<Order>();
        int lineCounter = 0;
        String line = br.readLine();
        while (line != null) {
            int errorCounter = 0;
            processCsvLine(customerList, orderList, line, lineCounter, errorCounter);
            line = br.readLine();
            lineCounter += 1;
        }
        processLastCsvLine(customerList, orderList, 0);
    }

    /**
     * For the last lines we just need to send them over to the server
     */
    private void processLastCsvLine(List<Customer> customerList, List<Order> orderList, int errorCounter) throws Exception {
        try {
            client.importCustomer(customerList);
            client.importOrders(orderList);
        } catch (ServiceUnavailableException e) {
            LOG.warn("Importing fails at last entities, retry counter: {}, error: ", errorCounter, e);
            if (errorCounter == maxErrorRetries) {
                throw e;
            } else {
                processLastCsvLine(customerList, orderList, errorCounter + 1);
            }
        } catch (Exception e) {
            LOG.warn("Importing fails at last lines, retry counter: {}, error: ", errorCounter, e);
            throw e;
        }
    }

    private void processCsvLine(List<Customer> customerList, List<Order> orderList, String line, int lineCounter, int errorCounter) throws Exception {
        try {
            if (customerList.size() >= importThreshold) {
                // if the lists are too large, we flush them to server
                LOG.info("Sending objects to server");
                client.importCustomer(customerList);
                client.importOrders(orderList);
                customerList.clear();
                orderList.clear();
            } else if (canParse(line)) {
                // create entities from CSV line
                Customer customer = Customer.parseCustomerFromCsv(line);
                Order order = Order.parseOrderFromCsv(line, customer.getId());
                customerList.add(customer);
                orderList.add(order);
            }
        } catch (ServiceUnavailableException e) {
            LOG.warn("Importing fails at entity number: {}, retry counter: {}, error: ", lineCounter, errorCounter, e);
            if (errorCounter == maxErrorRetries) {
                throw e;
            } else {
                processCsvLine(customerList, orderList, line, lineCounter, errorCounter + 1);
            }
        } catch (Exception e) {
            LOG.warn("Importing fails at count: {}, retry counter: {}, line: {}, error: ", lineCounter, errorCounter, line, e);
            throw e;
        }
    }

    /**
     * Indicates if the line can be parse
     * to {@link Customer} and {@link Order} entities
     *
     * @param line CSV line to parse
     * @return true if this line is parseable without errors
     */
    private boolean canParse(String line) {
        return !line.startsWith("seq") && !line.trim().isEmpty();
    }

    @VisibleForTesting
    public void setClient(Client client) {
        this.client = client;
    }

    @VisibleForTesting
    public void setImportThreshold(int importThreshold) {
        this.importThreshold = importThreshold;
    }

    @VisibleForTesting
    public void setMaxErrorRetries(int maxErrorRetries) {
        this.maxErrorRetries = maxErrorRetries;
    }
}