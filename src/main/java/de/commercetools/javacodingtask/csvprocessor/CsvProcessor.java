package de.commercetools.javacodingtask.csvprocessor;

import java.io.BufferedReader;

/**
 * Process CSV file
 *
 * @author lojzatran
 */
public interface CsvProcessor {
    void processCsv(BufferedReader br) throws Exception;
}
